<?php
session_start();
include('db.php');

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $usuario = $_POST['usuario'];
    $password = $_POST['password'];

    $db = new db();
    $pdo = $db->conexion();

    if ($pdo instanceof PDO) {
        $stmt = $pdo->prepare("SELECT * FROM usuario WHERE usuario_usuario = :usuario AND `usuario-password` = :password");
        $stmt->bindParam(':usuario', $usuario);
        $stmt->bindParam(':password', $password);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user) {
            $_SESSION['usuario_id'] = $user['usuario_id'];
            $_SESSION['usuario_nombre'] = $user['usuario_nombre'];
            $_SESSION['usuario_tipo'] = $user['usuario_tipo'];
            
            echo "¡Bienvenido!: " . $user['usuario_tipo'];
        } else {
            echo "Credenciales incorrectas";
        }
    } else {
        echo "Error al conectar a la base de datos.";
    }
}
?>
